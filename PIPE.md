# Pipe Segment Configuration

_mandatory_

* `address`

  Address of the source

* `catalogue`

  The id of the target catalogue

_optional_

* `dialect`

  The API dialect. Either `ckan`or `dkan`. Default is `ckan`

* `pageSize`

  Default value is `100`

* `incremental`

  Requires `lastRun` set in pipe header

* `filters`

  A map of key value pairs to filter datasets

* `pulse`

  The pulse in milliseconds for emitting datasets into the pipe. Default is no pulse.

# Data Info Object

The data info object is part of the ...

* `total`

  Total number of datasets

* `counter`

  The number of this dataset

* `identifier`

  The unique identifier in the source of this dataset

* `catalogue`

  The id of the target catalogue

