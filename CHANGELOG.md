# ChangeLog

## 1.2.3 (2023-11-06)

**Fixed:**
* Run id cache (connector update)

**Changed:**
* Console log output pattern, including date now
* Send request circuit breaker timeout increased to 5 min

**Added:**
* An additional request sent timeout of 4 min

## 1.2.2 (2023-10-08)

**Fixed:**
* Missing final identifier list request

## 1.2.1 (2023-08-25)

**Fixed:**
* Startup completion

## 1.2.0 (2023-08-25)

**Added:**
* Ability to config basic auth in pipe segment

## 1.1.4 (2022-12-24)

**Changed:**
* Load buildInfo once for faster health check
* Make importing verticle to an event loop verticle 

## 1.1.3 (2022-10-10)

**Fixed:**
* CI

## 1.1.2 (2022-10-10)

**Changed:**
* Lib updates

## 1.1.1 (2021-06-05)

**Added:**
* Use a circuit breaker

## 1.1.0 (2021-01-27)

**Changed:**
* Switched to Vert.x 4.0.0

**Fixed:**
* Failing on more response codes

## 1.0.2 (2020-06-18)

**Changed:**
* Pipe startTime

## 1.0.1 (2020-02-28)

**Changed:**
* Update connector lib
* Forward via separate verticle

## 1.0.0 (2019-11-08)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json
* `PIVEAU_LOG_LEVEL` for configuring general log level
* Change listener for configuration
* `sendHash` for configuring optional hash calculation in pipe
* `sedHash` to config schema

**Changed:**
* `PIVEAU_` prefix to logstash configuration environment variables
* Hash is optional and calculation is based on canonical algorithm
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Fixed:**
* Updated all dependencies

## 0.1.1 (2019-05-19)

**Fixed:**
* Don't use catalogue id as search filter

## 0.1.0 (2019-05-17)

**Added:**
* `catalogue` read from configuration and pass it to the info object
* Environment `PIVEAU_IMPORTING_SEND_LIST_DELAY` for a configurable delay
* `sendListDelay` pipe configuration option

**Changed:**
* Readme

**Removed:**
* `mode` configuration and fetchIdentifier

## 0.0.1 (2019-05-03)

Initial release
