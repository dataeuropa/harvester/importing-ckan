package io.piveau.importing;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@DisplayName("Testing the importer")
@ExtendWith(VertxExtension.class)
class ImportingTest {

    @BeforeEach
    void startImporter(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new MainVerticle(), testContext.succeeding(response -> testContext.verify(testContext::completeNow)));
    }

    @Test
    @Disabled("Needs to be redesigned due to its asynchronous nature")
    @DisplayName("pipe receiving")
    void sendPipe(Vertx vertx, VertxTestContext testContext) {
        Checkpoint checkpoint = testContext.checkpoint(2);
        JsonObject pipe = new JsonObject(vertx.fileSystem().readFileBlocking("test-pipe.json").toString());
        WebClient.create(vertx).postAbs("http://localhost:8080/pipe")
                .putHeader("Content-type", "application/json")
                .sendJsonObject(pipe, ar -> {
                    if (ar.succeeded()) {
                        checkpoint.flag();
                    } else {
                        testContext.failNow(ar.cause());
                    }
                });
    }

}
