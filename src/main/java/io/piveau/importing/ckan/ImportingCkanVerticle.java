package io.piveau.importing.ckan;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.importing.ckan.dialect.Dialect;
import io.piveau.importing.ckan.response.CkanError;
import io.piveau.importing.ckan.response.CkanResponse;
import io.piveau.pipe.PipeContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportingCkanVerticle extends AbstractVerticle {

    private static final String CATALOGUE_INFO_FIELD_NAME = "catalogue";

    public static final String ADDRESS = "io.piveau.pipe.importing.ckan.queue";

    private WebClient client;

    private CircuitBreaker circuitBreaker;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx, new WebClientOptions().setKeepAlive(false));

        circuitBreaker = CircuitBreaker.create("import", vertx, new CircuitBreakerOptions()
                        .setMaxRetries(2).setTimeout(360000))
                .retryPolicy((t, c) -> c * 1000L);

        startPromise.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().info("Import started");

        fetchPage(0, pipeContext, new ArrayList<>());
    }

    private void fetchPage(int offset, PipeContext pipeContext, List<String> identifiers) {
        JsonObject segmentConfig = pipeContext.getConfig();
        int pageSize = segmentConfig.getInteger("pageSize", 100);
        String address = segmentConfig.getString("address");

        Dialect dialect = Dialect.create(segmentConfig.getString("dialect", "ckan"));
        HttpRequest<Buffer> request = dialect.createRequest(client, address, offset, pageSize);

        if (segmentConfig.containsKey("filters")) {
            JsonObject filters = segmentConfig.getJsonObject("filters");
            filters.getMap().forEach((key, value) -> request.addQueryParam(key, value.toString()));
        }

        if (segmentConfig.containsKey("auth")) {
            JsonObject auth = segmentConfig.getJsonObject("auth", new JsonObject());
            if (auth.containsKey("basic")) {
                JsonObject basic = auth.getJsonObject("basic", new JsonObject());
                String username = basic.getString("username", "");
                String password = basic.getString("password", "");
                request.basicAuthentication(username, password);
            }
        }

        Boolean incremental = segmentConfig.getBoolean("incremental", false);
        if (Boolean.TRUE.equals(incremental)) {
            Date lastRun = pipeContext.getPipe().getHeader().getLastRun();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            request.addQueryParam("q", "metadata_modified:[" + dateFormat.format(lastRun) + "%20TO%20*]");
        }

        int pulse = segmentConfig.getInteger("pulse", 0);

        circuitBreaker.<HttpResponse<Buffer>>execute(promise -> request.timeout(240000).send()
                        .onSuccess(response -> {
                            if (response.statusCode() == 200) {
                                promise.complete(response);
                            } else {
                                promise.fail(response.statusMessage());
                            }
                        })
                        .onFailure(promise::fail))
                .onSuccess(response -> {
                    if (response.statusCode() == 200) {
                        CkanResponse ckanResponse = new CkanResponse(response.bodyAsJsonObject());
                        if (ckanResponse.isSuccess()) {
                            JsonArray results = dialect.getResult(ckanResponse);
                            final int total = dialect.getCount(ckanResponse);

                            boolean more = (total == -1 || offset < total) && !results.isEmpty();

                            if (pulse > 0) {
                                vertx.setPeriodic(pulse, l -> {
                                    JsonObject dataset = (JsonObject) results.remove(0);
                                    forwardDataset(dataset, pipeContext, total, identifiers);

                                    if (results.isEmpty()) {
                                        vertx.cancelTimer(l);
                                        if (more) {
                                            fetchPage(offset + pageSize, pipeContext, identifiers);
                                        } else {
                                            sendIdentifierList(identifiers, pipeContext);
                                            pipeContext.log().info("Import metadata finished");
                                        }
                                    }
                                });
                            } else {
                                results.forEach(object -> forwardDataset((JsonObject) object, pipeContext, total, identifiers));
                                if (more) {
                                    fetchPage(offset + pageSize, pipeContext, identifiers);
                                } else {
                                    sendIdentifierList(identifiers, pipeContext);
                                    pipeContext.log().info("Import metadata finished");
                                }
                            }
                        } else {
                            CkanError ckanError = ckanResponse.getError();
                            pipeContext.setFailure(ckanError.getMessage());
                        }
                    } else {
                        pipeContext.setFailure(response.statusMessage());
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private void forwardDataset(JsonObject dataset, PipeContext pipeContext, int total, List<String> identifiers) {
        pipeContext.getConfig().getString(CATALOGUE_INFO_FIELD_NAME);
        identifiers.add(dataset.getString("name"));
        ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                .put("total", total)
                .put("counter", identifiers.size())
                .put("identifier", dataset.getString("name"))
                .put(CATALOGUE_INFO_FIELD_NAME, pipeContext.getConfig().getString(CATALOGUE_INFO_FIELD_NAME));
        pipeContext.setResult(dataset.encodePrettily(), "application/json", dataInfo).forward();
        pipeContext.log().info("Dataset imported: {}", dataInfo);
    }

    private void sendIdentifierList(List<String> identifiers, PipeContext pipeContext) {
        int delay = pipeContext.getConfig().getInteger("sendListDelay", 8000);
        vertx.setTimer(delay, t -> {
            // give last datasets a head start with a chance to arrive at target
            ObjectNode info = new ObjectMapper().createObjectNode()
                    .put("content", "identifierList")
                    .put(CATALOGUE_INFO_FIELD_NAME, pipeContext.getConfig().getString(CATALOGUE_INFO_FIELD_NAME));
            pipeContext.setResult(new JsonArray(identifiers).encodePrettily(), "application/json", info).forward();
        });
    }

}
