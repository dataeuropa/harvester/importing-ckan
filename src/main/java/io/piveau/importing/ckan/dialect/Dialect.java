/*
 * Copyright (c) 2023. European Commission
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package io.piveau.importing.ckan.dialect;

import io.piveau.importing.ckan.response.CkanResponse;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;

public interface Dialect {

    HttpRequest<Buffer> createRequest(WebClient client, String address, int pageStart, int pageSize);

    JsonArray getResult(CkanResponse response);

    int getCount(CkanResponse response);

    static Dialect create(String dialect) {
        if ("dkan".equals(dialect)) {
            return new DkanDialect();
        } else {
            return new CkanDialect();
        }
    }

}
