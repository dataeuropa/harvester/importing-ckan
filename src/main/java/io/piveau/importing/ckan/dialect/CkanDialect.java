/*
 * Copyright (c) 2023. European Commission
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package io.piveau.importing.ckan.dialect;

import io.piveau.importing.ckan.response.CkanResponse;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;

public class CkanDialect implements Dialect {

    @Override
    public HttpRequest<Buffer> createRequest(WebClient client, String address, int pageStart, int pageSize) {
        HttpRequest<Buffer> request = client.getAbs(address + "/api/3/action/package_search");
        request.addQueryParam("rows", String.valueOf(pageSize)).addQueryParam("start", String.valueOf(pageStart));
        return request;
    }

    @Override
    public JsonArray getResult(CkanResponse response) {
        JsonObject content = response.getObjectResult().getContent();
        return content.getJsonArray("results", content.getJsonArray("result", new JsonArray()));
    }

    @Override
    public int getCount(CkanResponse response) {
        JsonObject content = response.getObjectResult().getContent();
        return content.getInteger("count", -1);
    }

}
